%include "vm.inc"

; flag = GLUG{viRTuAl_r@Nd0MiZ3R}
    

; LE(GLUG) ^ LE({viR) == 0x153c3a3c
vmovi vr8, 0x153c3a3c
vxor vr1, vr0
vxor vr1, vr8; result in vr1

; LE(GLUG) + 0x2 == 0x47554c57
vadd vr0, 0x10
vmovi vr8, 0x47554c57
vxor vr0, vr8; result in vr0

; LE(TuAl) - 0x20 == 0x6c417534
vsub vr2, 0x20
vmovi vr8, 0x6c417534
vxor vr2, vr8; result in vr2

; LE(_r@N) & 0xFFFFFFFF == 0x4e40725f
vmovi vr8, 0xFFFFFFFF
vand vr3, vr8
vmovi vr8, 0x4e40725f
vxor vr3, vr8; result in vr3

; LE(d0Mi) ^ vrand(0x22) == 0x694d3064
vrand vr8, 0x125
vxor vr4, vr8
vmovi vr8, 0x694d3046
vxor vr4, vr8; result in vr4

;LE(Z3R})+0x2 ^ vrand(0x1) == 0x7d52335d
vrand vr8, 0xF
vadd vr5, 0x2
vxor vr5, vr8
vmovi vr8, 0x7d52335d
vxor vr5, vr8;result is in vr5



vor vr0, vr1
vor vr0, vr2
vor vr0, vr3
vor vr0, vr4
vor vr0, vr5



vend