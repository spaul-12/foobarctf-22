%define  vr0 0
%define  vr1 1
%define  vr2 2
%define  vr3 3
%define  vr4 4
%define  vr5 5
%define  vr6 6
%define  vr7 7
%define  vr8 8
%define  vr9 9
%define  vr10 10
%define  vr11 11
%define  vr12 12
%define  vr13 13
%define  vr14 14
%define  vr15 15


; Corresponds to the #define in c and c++

%macro vxor 2 ; 2 is the number of arguments
    db 0x00, %1, %2; 0x00 --> opcode, %1 --> 1st argument, %2 --> 2nd argument; db is used to emit values
%endmacro

%macro vand 2 
    db 0x01, %1, %2
%endmacro

%macro vor 2 
    db 0x02, %1, %2
%endmacro

%macro vmov 2 
    db 0x03, %1, %2
%endmacro

%macro vmovi 2 
    db 0x04, %1;
    dd %2; dd is needed for movi as 2nd argument is 32-bit number
%endmacro

%macro vleft 2 
    db 0x05, %1;
    dd %2; dd is needed for movi as 2nd argument is 32-bit number
%endmacro

%macro vright 2 
    db 0x06, %1;
    dd %2; dd is needed for movi as 2nd argument is 32-bit number
%endmacro

%macro vadd 2 
    db 0x07, %1;
    dd %2;
%endmacro

%macro vsub 2 
    db 0x08, %1;
    dd %2; 
%endmacro

%macro vrand 2 
    db 0x09, %1;
    dd %2; 
%endmacro

%macro vend 0 
    db 0xff
%endmacro

; Above is the assembly definition, next make your program in this language
; File for the same is keycheck.vasm
